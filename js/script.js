/*----------------- Our service tabs -----------------*/
$(".tabs_caption li").on("click", function () {
    $(this).addClass("tabs_caption-active").siblings().removeClass("tabs_caption-active");
    const index = $(this).index();
    $(".tabs_content").removeClass("tabs_content-active").eq(index).addClass("tabs_content-active");
});

/*----------------- Load more button (Our amazing work) -----------------*/
$(document).ready(function () {
    $(".amazing-work-img-block").slice(0, 12).show();
    $("#load").on("click", function (e) {
        counter++;
        $(".loader").removeClass('hide');
        setTimeout(function () {

            e.preventDefault();
            $(".amazing-work-img-block:hidden").slice(0, 12).slideDown();
            if ($(".amazing-work-img-block:visible").length == 36) {
                const load = $('#load');
                load.hide();
            }
            $(".loader").addClass('hide');
        }, 2000);
        // console.log(counter);
    });
});

/*----------------- Our amazing work -----------------*/
// Filter Our amazing work with Vanilla JS
const filterAmazingWork = document.querySelectorAll(".amazing-work-tabs .amazing-work-tab");
let counter = 1;
filterAmazingWork.forEach(function (item) {
    item.addEventListener("click", function (e) {
        const parent = this.parentElement;
        if (this.dataset.filter == '*') {   //When you go to any section (like Graphic Design, Web Design, etc.) and then return to sectiction All, it still has exactly the same amount as it was originally

            const allImages = [...document.getElementsByClassName('amazing-work-img-block')];
            allImages.forEach(function (image) {
                image.style.display = "none";
            });
            const firstImages = allImages.slice(0, 12 * counter);
            firstImages.forEach(function (image) {
                image.style.display = "block";
            });
            for (let i = 0; i < parent.children.length; i++) {
                parent.children[i].classList.remove("amazing-work-tab-active");
            }
            ;
            this.classList.add("amazing-work-tab-active");
            return;
        }
        for (let i = 0; i < parent.children.length; i++) {
            parent.children[i].classList.remove("amazing-work-tab-active");
        }
        this.classList.add("amazing-work-tab-active");
        const images = document.getElementsByClassName("amazing-work-img-block");
        for (let i = 0; i < images.length; i++) {
            images[i].style.display = "none";
        }
        const selector = this.dataset.filter;
        const showImages = document.querySelectorAll(`${selector}.amazing-work-img-block`);
        for (let i = 0; i < showImages.length; i++) {
            showImages[i].style.display = "block";
        }
    });
});

/*----------------- What people say - Swiper -----------------*/
const galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 36,
    slidesPerView: 4,
    loop: false,
    freeMode: false,
    loopedSlides: 4,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});
const galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 36,
    loop: true,
    loopedSlides: 4,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs,
    },
});

/*----------------- Gallery of best images - Masonry -----------------*/
$(document).ready(function () {
    $(".item-masonry").hide();
    $(".item-masonry").slice(0, 12).show();
    $("#load-best-gallery").on("click", function (e) {
        e.preventDefault();
        $(".loader").removeClass('hide');
        setTimeout(function () { //preloader
            useMasonry();
            $(".item-masonry").show(); //show hidde images
            $(".loader").addClass('hide');
        }, 2000);
    });
});
useMasonry();

function useMasonry() {
    $(document).ready(function () {
        jQuery('.item-masonry').hover(
            function () {
                $(this).find(".cover-item-gallery").fadeIn();
            },
            function () {
                $(this).find(".cover-item-gallery").fadeOut();
            }
        );
        var sizer = '.sizer4';
        var container = $('#gallery');
        container.imagesLoaded(function () {
            container.masonry({
                itemSelector: '.item-masonry',
                columnWidth: sizer,
                percentPosition: true
            });
            $('.grid').masonry({
                itemSelector: '.grid-item',
                // columnWidth: 30%,
                gutter: 20,
                percentPosition: true
            });
        });
    });
}








